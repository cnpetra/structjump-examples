param baseMVA;
param nbus;
param ref_bus;
set BUS:={1..nbus};
set BUS_SUB:={1..4};

param nline;
set LINE:={1..nline};

param ngen;
set GEN:={1..ngen};

param Pmin{GEN};
param Pmax{GEN};
param Qmin{GEN};
param Qmax{GEN};
param Vmin{BUS};
param Vmax{BUS};
param Va_min{BUS};
param Va_max{BUS};

param Pg0{GEN};
param Qg0{GEN};
param Vm0{BUS};
param Va0{BUS};

var Pg{i in GEN}:=Pg0[i],  >=Pmin[i], <=Pmax[i];
var Qg{i in GEN}:=Qg0[i],  >=Qmin[i], <=Qmax[i];
var Vm{i in BUS}:=Vm0[i],  >=Vmin[i], <=Vmax[i];
var Va{i in BUS}:=Va0[i],  >=Va_min[i], <=Va_max[i];


param YffR{LINE};
param YffI{LINE};
param YttR{LINE};
param YttI{LINE};
param YftR{LINE};
param YftI{LINE};
param YtfR{LINE};
param YtfI{LINE};
param YshR{LINE};
param YshI{LINE};


set FromLines{BUS} within LINE;
set ToLines{BUS} within LINE;
param to{LINE};
param from{LINE};

param coeff_n{GEN};
param coeff_n1{GEN};
param coeff_n2{GEN};


#objective
minimize Total_Cost: sum{i in GEN} ( coeff_n2[i]*(baseMVA*Pg[i])^2 
                         +coeff_n1[i]*(baseMVA*Pg[i])
                     +coeff_n[i])
    ;


s.t. flow_balance_r{b in BUS}: 
        (sum{l in FromLines[b]}( YffR[l] ) + sum{ l in ToLines[b] }( YttR[l])  +  YshR[b] ) *Vm[b]^2  +
        sum{l in FromLines[b]} ( Vm[b]*Vm[to[l]] * ( YftR[l]*cos(Va[b] - Va[to[l]]) + YftI[l]*sin(Va[b] - Va[to[l]])) ) +
        sum{l in ToLines[b]} ( Vm[b]*Vm[from[l]] * ( YtfR[l]*cos(Va[b]-Va[from[l]]) + YtfI[l]*sin(Va[b] - Va[from[l]]  )  )  ) <= 0
        ;


s.t. flow_balance_i{b in BUS}: 
        (sum{l in FromLines[b]}( YffR[l]  ) + sum{l in ToLines[b]}(  YttR[l] ) + YshR[b] ) * Vm[b]^2 +
        sum{l in FromLines[b]}( Vm[b]*Vm[to[l]] * ( YftR[l]*cos(Va[b] - Va[to[l]])   + YftI[l]*sin(Va[b]-Va[to[l]])) ) +
        sum{l in ToLines[b]}( Vm[b]*Vm[from[l]] * ( YtfR[l]*cos(Va[b] - Va[from[l]]) + YtfI[l]*sin(Va[b]-Va[from[l]])) ) >= 0
        ;
 

