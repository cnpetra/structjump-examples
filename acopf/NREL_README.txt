################################
"Installation" instructions
https://confluence.exascaleproject.org/display/ADSE22/Installation

Those instructions are for an older version of Julia and JuMP
Instead, use
 > Julia 0.6.2  (any 0.6.x should work)
 > JuMP 0.18 (any 0.18.x should work)
 > For StructJuMP checkout commit 3a96b2fec830a6ffcbb63ed76f32b96c7c4f01df
       Author: Benoît Legat <benoit.legat@gmail.com>
       Date:   Fri Jun 2 14:32:33 2017 +0200

################################
Models to look at
 - acopf_main.jl acopf.jl -> translation in JuMP of matpower models
 - scopf_main.jl scopf.jl -> JuMP model SC-ACOPF (line contingencies)  variant of the above (in Cosmin's take)
 - scopf_stochDemand.jl -> JuMP model SC-ACOPF with line contingencies and "stochastic" demand scenarios per contingency
 - scopf_stochDemand_structjump.jl -> StructJuMP model variant of the above
 - scopf_stochDemand_structjump_busSlacks.jl -> StructJuMP model variant of the above with slacks at the buses
 - scopf_stochDemand_structjump_busSlacks_v2_withRestart.jl -> StructJuMP model variant of the above but with restart capabilities
