include("opfdata_feng.jl")
using StructJuMP, JuMP
using StructJuMPSolverInterface

type SCOPFData
  raw::RawData
  lines_off::Array
  #Float64::gener_ramp #generator ramp limit for contingency (percentage)
end


function scopf_solve(scopfmodel, scopfdata, solver, prof::Bool) 
  
  status = StructJuMPSolverInterface.sj_solve(scopfmodel;solver=solver, with_prof=prof)
  
  # getVarValue(scopfmodel)
  return scopfmodel,status
end

# Compute initial point for IPOPT based on the values provided in the case data
function scopf_compute_x0(opfdata)
  Pg=zeros(length(opfdata.generators)); 
  Qg=zeros(length(opfdata.generators)); 
  i=1
  for g in opfdata.generators
    # set the power levels in in between the bounds as suggested by matpower 
    # (case data also contains initial values in .Pg and .Qg - not used with IPOPT)
    Pg[i]=0.5*(g.Pmax+g.Pmin)
    Qg[i]=0.5*(g.Qmax+g.Qmin)
    i=i+1
  end
  @assert i-1==length(opfdata.generators)

  Vm=zeros(length(opfdata.buses)); i=1;
  for b in opfdata.buses
    # set the ini val for voltage magnitude in between the bounds 
    # (case data contains initials values in Vm - not used with IPOPT)
    Vm[i]=0.5*(b.Vmax+b.Vmin); 
    i=i+1
  end
  @assert i-1==length(opfdata.buses)

  # set all angles to the angle of the reference bus
  Va = opfdata.buses[opfdata.bus_ref].Va * ones(length(opfdata.buses))

  return Pg,Qg,Vm,Va
end


function scopf_init_x(scopfmodel,scopfdata,demandScens)
  raw = scopfdata.raw
  lines_off = scopfdata.lines_off
  for i in getLocalBlocksIds(scopfmodel)
    co = 1+div(i-1,demandScens)
    sc = 1+mod(i-1,demandScens)
    if(i==0)
      opfdata = opf_loaddata(raw)
      Pg0,Qg0,Vm0,Va0 = scopf_compute_x0(opfdata)
      extra0 = 0.0*Pg0 
      setvalue(getindex(scopfmodel, :Pg), Pg0)
      setvalue(getindex(scopfmodel, :Pramp), extra0)  
      setvalue(getindex(scopfmodel, :Qg), Qg0)
      setvalue(getindex(scopfmodel, :Qramp), extra0)  
      setvalue(getindex(scopfmodel, :Vm), Vm0)
      setvalue(getindex(scopfmodel, :Va), Va0)
    else
      mm = getchildren(scopfmodel)[i]
      opfdata_c=opf_loaddata(raw,lines_off[co]) 
      Pg0,Qg0,Vm0,Va0 = scopf_compute_x0(opfdata_c)
      extra0 = 0.0*Pg0
      setvalue(getindex(mm, :Pramp), extra0)
      setvalue(getindex(mm, :Qramp), extra0)  
      setvalue(getindex(mm, :Vm), Vm0)
      setvalue(getindex(mm, :Va), Va0)
    end
  end
end


#num_scens -> number of demand scenarios per contingency
function scopf_model(raw::RawData, num_scens)
  opfdata = opf_loaddata(raw) #load root node
  
  line_args = ARGS[5:end]
  lines_off=Array{Line}(length(line_args))

  for l in 1:length(lines_off)
    idx = parse(Int,line_args[l])
    lines_off[l] = opfdata.lines[idx]
    lines_off[l].idx = idx
  end
  sd = SCOPFData(raw,lines_off)

  ncont = length(sd.lines_off)

  nscen=ncont*num_scens
  opfmodel = StructuredModel(num_scenarios=nscen)

  #shortcuts for compactness
  lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
  busIdx = opfdata.BusIdx; FromLines = opfdata.FromLines; ToLines = opfdata.ToLines; BusGeners = opfdata.BusGenerators;
  nbus  = length(buses); nline = length(lines); ngen  = length(generators);
 
  #branch admitances
  YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)

  println("Considering ", ncont, " contingencies and ", num_scens, " scenarios per contingency")

  # first stage powers
  @variable(opfmodel, generators[i].Pmin <= Pg[i=1:ngen] <= generators[i].Pmax)
  @variable(opfmodel, generators[i].Qmin <= Qg[i=1:ngen] <= generators[i].Qmax)

  r=0.5;
  @variable(opfmodel, -r*r*generators[i].Pmax<=Pramp[i=1:ngen]<=r*r*generators[i].Pmax)
  @variable(opfmodel, -r*r*generators[i].Qmax<=Qramp[i=1:ngen]<=r*r*generators[i].Qmax)	

  @variable(opfmodel, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
  @variable(opfmodel, Va[1:nbus])
  
  #fix the voltage angle at the reference bus
  setlowerbound(Va[opfdata.bus_ref], buses[opfdata.bus_ref].Va)
  setupperbound(Va[opfdata.bus_ref], buses[opfdata.bus_ref].Va)

  @NLobjective(opfmodel, Min, sum( generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]+Pramp[i]))^2 
			             +generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]+Pramp[i]))
				     +generators[i].coeff[generators[i].n  ] for i=1:ngen)/(1+nscen))
  #################################
  ## first stage constraints
  #################################
  #max ramp constraints
  @constraint(opfmodel, rampP[i=1:ngen], generators[i].Pmin <= Pg[i] + Pramp[i] <= generators[i].Pmax)
  @constraint(opfmodel, rampQ[i=1:ngen], generators[i].Qmin <= Qg[i] + Qramp[i] <= generators[i].Qmax)

  # power flow balance - first stage
  for b in 1:nbus
    #real part
    @NLconstraint(opfmodel, 
      ( sum( YffR[l] for l in FromLines[b]) + sum( YttR[l] for l in ToLines[b]) + YshR[b] ) * Vm[b]^2 
      + sum( Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )  
      + sum( Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   ) 
      - ( sum(baseMVA*(Pg[g]+Pramp[g]) for g in BusGeners[b]) - buses[b].Pd ) / baseMVA      # Sbus part
      ==0)

    #imaginary part
    @NLconstraint(opfmodel,
      ( sum(-YffI[l] for l in FromLines[b]) + sum(-YttI[l] for l in ToLines[b]) - YshI[b] ) * Vm[b]^2 
      + sum( Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )
      + sum( Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   )
      - ( sum(baseMVA*(Qg[g]+Qramp[g]) for g in BusGeners[b]) - buses[b].Qd ) / baseMVA      #Sbus part
      ==0)
  end

  # branch/lines flow limits  - first stage
  nlinelim=0
  for l in 1:nline
    if lines[l].rateA!=0 && lines[l].rateA<1.0e10
      nlinelim += 1
      flowmax=(lines[l].rateA/baseMVA)^2

      #branch apparent power limits (from bus)
      Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
      Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
      @NLconstraint( opfmodel,
	  Vm[busIdx[lines[l].from]]^2 *
	  ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
	  + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
	  ) 
          - flowmax <=0)

        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint(
          opfmodel,
    	  Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
          + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          )
          - flowmax <=0)
    end
  end # of for: branch power limits - first stage

  @show "Lines with limits  ", nlinelim

  #######################################
  ### the submodel/ second-stage blocks
  ######################################
  # add second stage constraints
  for block in getLocalChildrenIds(opfmodel) #for block=0:ncont*num_scens
    co = 1+div(block-1,num_scens)
    sc = 1+mod(block-1,num_scens)

    # @declare_second_stage(opfmodel, c, begin
    opfmodel_c = StructuredModel(parent=opfmodel,id=block)
    opfdata_c  = opf_loaddata(raw, sd.lines_off[co]) 
    
    #shortcuts for compactness
    lines = opfdata_c.lines; buses = opfdata_c.buses; generators = opfdata_c.generators; baseMVA = opfdata_c.baseMVA
    busIdx = opfdata_c.BusIdx; FromLines = opfdata_c.FromLines; ToLines = opfdata_c.ToLines; BusGeners = opfdata_c.BusGenerators
    nbus  = length(buses); nline = length(lines); ngen  = length(generators)
    YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(opfdata_c.lines, opfdata_c.buses, opfdata_c.baseMVA)
    #############################
    # second stage variables
    #############################
    r=0.5;
    @variable(opfmodel_c, -r*generators[i].Pmax<=Pramp[i=1:ngen]<=r*generators[i].Pmax)
    @variable(opfmodel_c, -r*generators[i].Qmax<=Qramp[i=1:ngen]<=r*generators[i].Qmax)
    @variable(opfmodel_c, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
    @variable(opfmodel_c, Va[1:nbus])

    #fix the voltage angle at the reference bus
    setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
    setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)

    @NLobjective(opfmodel_c, Min, sum( generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]+Pramp[i]))^2 
			             +generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]+Pramp[i]))
				     +generators[i].coeff[generators[i].n  ] for i=1:ngen)/(1+nscen))

    # max ramping
    @NLconstraint(opfmodel_c, exP[i=1:ngen], generators[i].Pmin <= Pg[i] + Pramp[i] <= generators[i].Pmax)
    @NLconstraint(opfmodel_c, exQ[i=1:ngen], generators[i].Qmin <= Qg[i] + Qramp[i] <= generators[i].Qmax)

    #fix the seed to the demand scenario -> line contingencies will have the same demand scenarios
    srand(97*sc)
    randDemandFactorP = 1+0.025*randn(nbus)
    randDemandFactorQ = 1+0.025*randn(nbus)


    # power flow balance
    for b in 1:nbus
      #real part
      @NLconstraint( opfmodel_c, 
        ( sum( YffR[l] for l in FromLines[b]) + sum( YttR[l] for l in ToLines[b]) + YshR[b] ) * Vm[b]^2 
        + sum(  Vm[b]*Vm[busIdx[lines[l].to]]*( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]) 
              + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )  
        + sum(  Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) 
              + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   ) 
        -(sum(baseMVA*(Pg[g]+Pramp[g]) for g in BusGeners[b]) - buses[b].Pd*randDemandFactorP[b]) / baseMVA      # Sbus part
        ==0)

      #imaginary part
      @NLconstraint( opfmodel_c,
        ( sum(-YffI[l] for l in FromLines[b]) + sum(-YttI[l] for l in ToLines[b]) - YshI[b] ) * Vm[b]^2 
        + sum(  Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]) 
              + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )
        + sum( Vm[b]*Vm[busIdx[lines[l].from]] *(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) 
              + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   )
        -(sum(baseMVA*(Qg[g]+Qramp[g]) for g in BusGeners[b]) - buses[b].Qd*randDemandFactorQ[b]) / baseMVA      #Sbus part
        ==0)
    end # of for: power flow balance loop



    # branch/lines flow limits

    nlinelim=0
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        nlinelim += 1
        flowmax=(lines[l].rateA/baseMVA)^2

        #branch apparent power limits (from bus)
        Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
        Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
        @NLconstraint( opfmodel_c,
	  Vm[busIdx[lines[l].from]]^2 *
	  ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
	  + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
	  )  - flowmax <=0)

        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint( opfmodel_c,
    	  Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
          + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          ) - flowmax <=0)
      end
    end # of for: branch power limits


    @printf("Contingency %d -> Buses: %d  Lines: %d  Generators: %d\n", co, nbus, nline, ngen)
    println("     lines with limits:  ", nlinelim)
  

  end # end of for: contingencies x demand_scens

 
  return opfmodel, sd
end

  #######################################################
  
  #values = zeros(2*nbus+2*ngen) 
  ## values[1:2*nbus+2*ngen] = readdlm("/sandbox/petra/work/installs/matpower5.1/vars2.txt")
  #values[1:2*nbus+2*ngen] = readdlm("/sandbox/petra/work/installs/matpower5.1/vars3_9241.txt")
  #d = JuMP.NLPEvaluator(opfmodel)
  #MathProgBase.initialize(d, [:Jac])

  #g = zeros(2*nbus+2*nlinelim)
  #MathProgBase.eval_g(d, g, values)
  #println("f=", MathProgBase.eval_f(d,values))
 
  #gmat=zeros(2*nbus+2*nlinelim)
  #gmat[1:end] = readdlm("/sandbox/petra/work/installs/matpower5.1/cons3_9241.txt")
  #println("diff: ", norm(gmat-g))

  #println(opfmodel)

  #############################################################

function scopf_outputAll(opfmodel, scopf_data)
  #shortcuts for compactness
  sd=scopf_data; opf_data=sd.opfdata
  lines = opf_data.lines; buses = opf_data.buses; generators = opf_data.generators; baseMVA = opf_data.baseMVA
  busIdx = opf_data.BusIdx; FromLines = opf_data.FromLines; ToLines = opf_data.ToLines; BusGeners = opf_data.BusGenerators;

  nbus  = length(buses); nline = length(lines); ngen  = length(generators)

  # OUTPUTING
  println("Objective value: ", getobjectivevalue(opfmodel), "USD/hr")
  VM=getvalue(getindex(opfmodel,:Vm)); VA=getvalue(getindex(opfmodel,:Va));
  PG=getvalue(getindex(opfmodel,:Pg)); QG=getvalue(getindex(opfmodel,:Qg));

  VM=VM[:,0]; VA=VA[:,0]; #base case

  EX=getvalue(getindex(opfmodel,:Pramp));
  EX=EX[:,0];

  # printing the first stage variables
  println("============================= BUSES ==================================")
  println("  Generator  |  ramp ")   # |    P (MW)     Q (MVAr)")  #|         (load)   ")  
  println("----------------------------------------------------------------------")
  for i in 1:ngen
      @printf("  %10d | %6.2f \n",generators[i].bus, EX[i])
  end
  println("\n")

  println("============================= BUSES ==================================")
  println("  BUS    Vm     Va   |   Pg (MW)    Qg(MVAr) ")   # |    P (MW)     Q (MVAr)")  #|         (load)   ") 
  
  println("                     |     (generation)      ") 
  println("----------------------------------------------------------------------")
  for i in 1:nbus
    @printf("%4d | %6.2f  %6.2f | %s  | \n",
	    buses[i].bus_i, VM[i], VA[i]*180/pi, 
	    length(BusGeners[i])==0?"   --          --  ":@sprintf("%7.2f     %7.2f", baseMVA*PG[BusGeners[i][1]], baseMVA*QG[BusGeners[i][1]]))
  end   
  println("\n")

  within=20 # percentage close to the limits
  
  
  nflowlim=0
  for l in 1:nline
    if lines[l].rateA!=0 && lines[l].rateA<1.0e10
      nflowlim += 1
    end
  end
  return

  if nflowlim>0 
    println("Number of lines with flow limits: ", nflowlim)

    optvec=zeros(2*nbus+2*ngen)
    optvec[1:ngen]=PG
    optvec[ngen+1:2*ngen]=QG
    optvec[2*ngen+1:2*ngen+nbus]=VM
    optvec[2*ngen+nbus+1:2*ngen+2*nbus]=VA

    d = JuMP.NLPEvaluator(opfmodel)
    MathProgBase.initialize(d, [:Jac])

    consRhs = zeros(2*nbus+2*nflowlim)
    MathProgBase.eval_g(d, consRhs, optvec)  


    #println(consRhs)

    @printf("================ Lines within %d %s of flow capacity ===================\n", within, "\%")
    println("Line   From Bus    To Bus    At capacity")

    nlim=1
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        flowmax=(lines[l].rateA/baseMVA)^2
        idx = 2*nbus+nlim
        
        if( (consRhs[idx]+flowmax)  >= (1-within/100)^2*flowmax )
          @printf("%3d      %3d      %3d        %5.3f%s\n", l, lines[l].from, lines[l].to, 100*sqrt((consRhs[idx]+flowmax)/flowmax), "\%" ) 
          #@printf("%7.4f   %7.4f    %7.4f \n", consRhs[idx], consRhs[idx]+flowmax,  flowmax)
        end
        nlim += 1
      end
    end
  end

  return 
end


# Compute initial point for IPOPT based on the values provided in the case data
function acopf_initialPt_IPOPT(opfdata)
  Pg=zeros(length(opfdata.generators)); Qg=zeros(length(opfdata.generators)); i=1
  for g in opfdata.generators
    # set the power levels in in between the bounds as suggested by matpower 
    # (case data also contains initial values in .Pg and .Qg - not used with IPOPT)
    Pg[i]=0.5*(g.Pmax+g.Pmin)
    Qg[i]=0.5*(g.Qmax+g.Qmin)
    i=i+1
  end
  @assert i-1==length(opfdata.generators)

  Vm=zeros(length(opfdata.buses)); i=1;
  for b in opfdata.buses
    # set the ini val for voltage magnitude in between the bounds 
    # (case data contains initials values in Vm - not used with IPOPT)
    Vm[i]=0.5*(b.Vmax+b.Vmin); 
    i=i+1
  end
  @assert i-1==length(opfdata.buses)

  # set all angles to the angle of the reference bus
  Va = opfdata.buses[opfdata.bus_ref].Va * ones(length(opfdata.buses))

  return Pg,Qg,Vm,Va
end


function main()
  mid, nprocs = getMyRank();
  solver = ARGS[1]; 
  prof = eval(parse(ARGS[2])); 
  
  if prof
    tic()  #t_user_model_loading
  end

  if prof
    tic() #t_rawdata_loading
  end

  raw = loadrawdata(ARGS[3]); 
  if prof
    t_rawdata_loading = toq()
    @message @sprintf("t_rawdata_loading %s", t_rawdata_loading)
  end

  # these are # of demand scenario per contingency
  num_demand_scens = parse(Int, ARGS[4]); 

  scopfmodel, scopfdata = scopf_model(raw, num_demand_scens)
  # 
  # Initial point - needed especially for pegase cases
  #
  srand(10)
  scopf_init_x(scopfmodel,scopfdata,num_demand_scens)

  if prof
    t_user_model_loading = toq()
    @message @sprintf(" t_user_model_loading %s ", t_user_model_loading)
  end
 
  if prof
    tic() # t_solve
  end

  model,status = scopf_solve(scopfmodel,scopfdata, solver, prof)

  if prof
    t_solve = toq()
    @message @sprintf(" t_solve %s ", t_solve)
  end

  if status==:Optimal
    #acopf_outputAll(opfmodel,opfdata)
  end
end
#run the case 9 example with 2 contingencies (lines 2 and 8 off, the last two parameters) and 16 demand scenarios per contingency
### command line: julia scopf_stochDemand_structjump.jl Ipopt false data/case9 2  2 8
main()
