include("opfdata_feng.jl")
using StructJuMP, JuMP
using StructJuMPSolverInterface

type SCOPFData
  raw::RawData
  lines_off::Array
  #Float64::gener_ramp #generator ramp limit for contingency (percentage)
end


function scopf_solve(scopfmodel, scopfdata, solver, prof::Bool) 
  #return 0,0
  status = StructJuMPSolverInterface.sj_solve(scopfmodel;solver=solver, with_prof=prof)
  
  # getVarValue(scopfmodel)
  return scopfmodel,status
end

# Compute initial point for IPOPT based on the values provided in the case data
function scopf_compute_x0(opfdata)
  Pg=zeros(length(opfdata.generators)); 
  Qg=zeros(length(opfdata.generators)); 
  i=1
  for g in opfdata.generators
    # set the power levels in in between the bounds as suggested by matpower 
    # (case data also contains initial values in .Pg and .Qg - not used with IPOPT)
    #Pg[i]=(g.Pmin)
    #Qg[i]=(g.Qmin)

    Pg[i]=0.5*(g.Pmax+g.Pmin)
    Qg[i]=0.5*(g.Qmax+g.Qmin)
    i=i+1
  end
  @assert i-1==length(opfdata.generators)

  Vm=zeros(length(opfdata.buses)); i=1;
  for b in opfdata.buses
    # set the ini val for voltage magnitude in between the bounds 
    # (case data contains initials values in Vm - not used with IPOPT)
    Vm[i]=0.5*(b.Vmax+b.Vmin); 
    i=i+1
  end
  @assert i-1==length(opfdata.buses)

  # set all angles to the angle of the reference bus
  Va = opfdata.buses[opfdata.bus_ref].Va * ones(length(opfdata.buses))

  return Pg,Qg,Vm,Va
end


function scopf_init_x(scopfmodel,scopfdata,demandScens)

  raw = scopfdata.raw
  lines_off = scopfdata.lines_off
  for i in getLocalBlocksIds(scopfmodel)
    co = 1+div(i-1,demandScens)
    sc = 1+mod(i-1,demandScens)
    if(i==0)
      opfdata = opf_loaddata(raw)
      Pg0,Qg0,Vm0,Va0 = scopf_compute_x0(opfdata)
      extra0 = 0.0*Pg0 
      setvalue(getindex(scopfmodel, :Pg), Pg0)
      # setvalue(getindex(scopfmodel, :Pramp), extra0)  
      setvalue(getindex(scopfmodel, :Qg), Qg0)
      # setvalue(getindex(scopfmodel, :Qramp), extra0)  
      setvalue(getindex(scopfmodel, :Vm), Vm0)
      setvalue(getindex(scopfmodel, :Va), Va0)
      #setvalue(getindex(scopfmodel, :P_slack0), 0.0*Vm0)
      #setvalue(getindex(scopfmodel, :Q_slack0), 0.0*Vm0)  
    else
      mm = getchildren(scopfmodel)[i]
      opfdata_c=opf_loaddata(raw,lines_off[co]) 
      Pg0,Qg0,Vm0,Va0 = scopf_compute_x0(opfdata_c)
      extra0 = 0.0*Vm0
      setvalue(getindex(mm, :P_slack), 0.0*Vm0)
      setvalue(getindex(mm, :Q_slack), 0.0*Vm0)  
      setvalue(getindex(mm, :Vm), Vm0)
      setvalue(getindex(mm, :Va), Va0)
      setvalue(getindex(mm, :Pramp), 0.0*Pg0)
      setvalue(getindex(mm, :Qramp), 0.0*Qg0)
    end
  end
end

function scopf_init_x_fromFiles(scopfmodel,scopfdata,demandScens) 

  for i in getLocalBlocksIds(scopfmodel)
    co = 1+div(i-1,demandScens)
    sc = 1+mod(i-1,demandScens)

    if(i==0)
      PG0, QG0, VA0, VM0 = scopf_loadSolFromFile(i)  
      setvalue(getindex(scopfmodel, :Pg), PG0)
      setvalue(getindex(scopfmodel, :Qg), QG0)
      setvalue(getindex(scopfmodel, :Vm), VM0)
      setvalue(getindex(scopfmodel, :Va), VA0)
    else 
      mm = getchildren(scopfmodel)[i]
      P_slacki, Q_slacki, VAi, VMi, Prampi, Qrampi = scopf_loadSolFromFile(co)
      setvalue(getindex(mm, :P_slack), P_slacki)
      setvalue(getindex(mm, :Q_slack), Q_slacki)  
      setvalue(getindex(mm, :Vm), VMi)
      setvalue(getindex(mm, :Va), VAi)
      setvalue(getindex(mm, :Pramp), Prampi)
      setvalue(getindex(mm, :Qramp), Qrampi)
    end
  end
end


#num_scens -> number of demand scenarios per contingency
function scopf_model(raw::RawData, num_scens)
  opfdata = opf_loaddata(raw) #load root node
  
  line_args = ARGS[6:end]
  lines_off=Array{Line}(length(line_args))

  for l in 1:length(lines_off)
    idx = parse(Int,line_args[l])
    lines_off[l] = opfdata.lines[idx]
    lines_off[l].idx = idx
  end
  sd = SCOPFData(raw,lines_off)

  ncont = length(sd.lines_off)

  nscen=ncont*num_scens
  opfmodel = StructuredModel(num_scenarios=nscen)

  #shortcuts for compactness
  lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
  busIdx = opfdata.BusIdx; FromLines = opfdata.FromLines; ToLines = opfdata.ToLines; BusGeners = opfdata.BusGenerators;
  nbus  = length(buses); nline = length(lines); ngen  = length(generators);
 
  #branch admitances
  YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)

  println("Considering ", ncont, " contingencies and ", num_scens, " scenarios per contingency")

  # first stage powers
  @variable(opfmodel, generators[i].Pmin <= Pg[i=1:ngen] <= generators[i].Pmax)
  @variable(opfmodel, generators[i].Qmin <= Qg[i=1:ngen] <= generators[i].Qmax)

  #r=0.01;
  # @variable(opfmodel, -r*r*generators[i].Pmax<=Pramp[i=1:ngen]<=r*r*generators[i].Pmax)
  # @variable(opfmodel, -r*r*generators[i].Qmax<=Qramp[i=1:ngen]<=r*r*generators[i].Qmax)	

  @variable(opfmodel, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
  @variable(opfmodel, Va[1:nbus])

  #@variable(opfmodel, P_slack0[1:nbus])
  #@variable(opfmodel, Q_slack0[1:nbus])
  
  #fix the voltage angle at the reference bus
  setlowerbound(Va[opfdata.bus_ref], buses[opfdata.bus_ref].Va)
  setupperbound(Va[opfdata.bus_ref], buses[opfdata.bus_ref].Va)

  obj_scale=0.01;
  penalty_for_slack = 100.

  @NLobjective(opfmodel, Min, obj_scale*(sum( generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]))^2 
			             +generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]))
				     +generators[i].coeff[generators[i].n  ] for i=1:ngen))/ (1+nscen))
  # +penalty_for_slack*baseMVA*baseMVA*sum(P_slack0[i]*P_slack0[i] + Q_slack0[i]*Q_slack0[i] for i=1:nbus))/(1+nscen))
  #################################
  ## first stage constraints
  #################################
  #max ramp constraints
  @constraint(opfmodel, rampP[i=1:ngen], generators[i].Pmin <= Pg[i] <= generators[i].Pmax)
  @constraint(opfmodel, rampQ[i=1:ngen], generators[i].Qmin <= Qg[i] <= generators[i].Qmax)

  # power flow balance - first stage
  for b in 1:nbus
    #real part
    @NLconstraint(opfmodel, 
      ( sum( YffR[l] for l in FromLines[b]) + sum( YttR[l] for l in ToLines[b]) + YshR[b] ) * Vm[b]^2 
      + sum( Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )  
      + sum( Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   ) 
      - ( sum(baseMVA*(Pg[g]) for g in BusGeners[b]) - buses[b].Pd ) / baseMVA      # Sbus part
      ==0)
    #imaginary part
    @NLconstraint(opfmodel,
      ( sum(-YffI[l] for l in FromLines[b]) + sum(-YttI[l] for l in ToLines[b]) - YshI[b] ) * Vm[b]^2 
      + sum( Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )
      + sum( Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   )
      - ( sum(baseMVA*(Qg[g]) for g in BusGeners[b]) - buses[b].Qd ) / baseMVA      #Sbus part
      ==0)
  end

  # branch/lines flow limits  - first stage
  nlinelim=0
  for l in 1:0#nline
    if lines[l].rateA!=0 && lines[l].rateA<1.0e10
      nlinelim += 1
      flowmax=(lines[l].rateA/baseMVA)^2

      #branch apparent power limits (from bus)
      Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
      Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
      @NLconstraint( opfmodel,
	  Vm[busIdx[lines[l].from]]^2 *
	  ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
	  + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
	  ) 
          - flowmax <=0)

        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint(
          opfmodel,
    	  Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
          + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          )
          - flowmax <=0)
    end
  end # of for: branch power limits - first stage

  @show "Lines with limits  ", nlinelim

  #######################################
  ### the submodel/ second-stage blocks
  ######################################
  # add second stage constraints
  for block in getLocalChildrenIds(opfmodel) #for block=0:ncont*num_scens
    co = 1+div(block-1,num_scens)
    sc = 1+mod(block-1,num_scens)

    # @declare_second_stage(opfmodel, c, begin
    opfmodel_c = StructuredModel(parent=opfmodel,id=block)
    opfdata_c  = opf_loaddata(raw, sd.lines_off[co]) 
    
    #shortcuts for compactness
    lines = opfdata_c.lines; buses = opfdata_c.buses; generators = opfdata_c.generators; baseMVA = opfdata_c.baseMVA
    busIdx = opfdata_c.BusIdx; FromLines = opfdata_c.FromLines; ToLines = opfdata_c.ToLines; BusGeners = opfdata_c.BusGenerators
    nbus  = length(buses); nline = length(lines); ngen  = length(generators)
    YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(opfdata_c.lines, opfdata_c.buses, opfdata_c.baseMVA)
    #############################
    # second stage variables
    #############################
    r=0.15;
    @variable(opfmodel_c, -r*generators[i].Pmax<=Pramp[i=1:ngen]<=r*generators[i].Pmax)
    @variable(opfmodel_c, -r*generators[i].Qmax<=Qramp[i=1:ngen]<=r*generators[i].Qmax)
    @variable(opfmodel_c, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
    @variable(opfmodel_c, Va[1:nbus])
    @variable(opfmodel_c, P_slack[1:nbus]) 
    @variable(opfmodel_c, Q_slack[1:nbus]) 

    #fix the voltage angle at the reference bus
    setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
    setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)

    
    @NLobjective(opfmodel_c, Min, obj_scale*(sum( generators[i].coeff[generators[i].n-2]*(baseMVA*(Pramp[i]+Pg[i]))^2 
			             +generators[i].coeff[generators[i].n-1]*(baseMVA*(Pramp[i]+Pg[i]))
				     +generators[i].coeff[generators[i].n  ] for i=1:ngen)
             +penalty_for_slack*baseMVA*baseMVA*sum(P_slack[i]*P_slack[i] + Q_slack[i]*Q_slack[i] for i=1:nbus))/(1+nscen))

    # max ramping
    @NLconstraint(opfmodel_c, exP[i=1:ngen], generators[i].Pmin <= Pg[i] + Pramp[i] <= generators[i].Pmax)
    @NLconstraint(opfmodel_c, exQ[i=1:ngen], generators[i].Qmin <= Qg[i] + Qramp[i] <= generators[i].Qmax)

    #fix the seed to the demand scenario -> line contingencies will have the same demand scenarios
    srand(97*sc)
    randDemandFactorP = 1+0.025*randn(nbus)
    randDemandFactorQ = 1+0.025*randn(nbus)
    #randDemandFactorP = 1+0.0*randn(nbus)
    #randDemandFactorQ = 1+0.0*randn(nbus)


    # power flow balance
    for b in 1:nbus
      #real part
      @NLconstraint( opfmodel_c, 
        ( sum( YffR[l] for l in FromLines[b]) + sum( YttR[l] for l in ToLines[b]) + YshR[b] ) * Vm[b]^2 
        + sum(  Vm[b]*Vm[busIdx[lines[l].to]]*( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]) 
              + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )  
        + sum(  Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) 
              + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   ) 
        -(sum(baseMVA*(Pg[g]+Pramp[g]) for g in BusGeners[b]) - buses[b].Pd*randDemandFactorP[b]) / baseMVA
        + P_slack[b]==0)   #Sbus part

      #imaginary part
      @NLconstraint( opfmodel_c,
        ( sum(-YffI[l] for l in FromLines[b]) + sum(-YttI[l] for l in ToLines[b]) - YshI[b] ) * Vm[b]^2 
        + sum(  Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]) 
              + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )
        + sum( Vm[b]*Vm[busIdx[lines[l].from]] *(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) 
              + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   )
        -(sum(baseMVA*(Qg[g]+Qramp[g]) for g in BusGeners[b]) - buses[b].Qd*randDemandFactorQ[b]) / baseMVA
        + Q_slack[b]==0)    #Sbus part
    end # of for: power flow balance loop



    # branch/lines flow limits

    nlinelim=0
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        nlinelim += 1
        flowmax=(lines[l].rateA/baseMVA)^2

        #branch apparent power limits (from bus)
        Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
        Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
        @NLconstraint( opfmodel_c,
	  Vm[busIdx[lines[l].from]]^2 *
	  ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
	  + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
	  )  - flowmax <=0)

        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint( opfmodel_c,
    	  Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
          + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]
               *(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          ) - flowmax <=0)
      end
    end # of for: branch power limits


    #@printf("Contingency %d -> Buses: %d  Lines: %d  Generators: %d   lines with limits: %d\n", co, nbus, nline, ngen, nlinelim)
  

  end # end of for: contingencies x demand_scens

 
  return opfmodel, sd
end

  #######################################################
  
  #values = zeros(2*nbus+2*ngen) 
  ## values[1:2*nbus+2*ngen] = readdlm("/sandbox/petra/work/installs/matpower5.1/vars2.txt")
  #values[1:2*nbus+2*ngen] = readdlm("/sandbox/petra/work/installs/matpower5.1/vars3_9241.txt")
  #d = JuMP.NLPEvaluator(opfmodel)
  #MathProgBase.initialize(d, [:Jac])

  #g = zeros(2*nbus+2*nlinelim)
  #MathProgBase.eval_g(d, g, values)
  #println("f=", MathProgBase.eval_f(d,values))
 
  #gmat=zeros(2*nbus+2*nlinelim)
  #gmat[1:end] = readdlm("/sandbox/petra/work/installs/matpower5.1/cons3_9241.txt")
  #println("diff: ", norm(gmat-g))

  #println(opfmodel)

  #############################################################

function scopf_saveSolToFile(scopfmodel, scopfdata)
  raw = scopfdata.raw
  opfdata = opf_loaddata(raw)
  lines_off = scopfdata.lines_off
  #opf_data = opf_loaddata(raw)



  raw = scopfdata.raw
  lines_off = scopfdata.lines_off
  for i in getLocalBlocksIds(scopfmodel)
    if(i==0)
      # save first stage vars
      VM=getvalue(getindex(scopfmodel,:Vm)); VA=getvalue(getindex(scopfmodel,:Va));
      PG=getvalue(getindex(scopfmodel,:Pg)); QG=getvalue(getindex(scopfmodel,:Qg));

      println(PG)
      writedlm("sol0_PGQG.txt", [PG, QG]);
      writedlm("sol0_VaVm.txt", [VA, VM]);
    else
      mm = getchildren(scopfmodel)[i]
      P_slack = getvalue(getindex(mm,:P_slack)); Q_slack = getvalue(getindex(mm,:Q_slack)); 
      writedlm(@sprintf("sol%d_PQslacks.txt", i), [P_slack, Q_slack]);
      
      VM=getvalue(getindex(mm,:Vm)); VA=getvalue(getindex(mm,:Va));
      writedlm(@sprintf("sol%d_VaVm.txt", i), [VA, VM]);

      pramp = getvalue(getindex(mm,:Pramp)); qramp=getvalue(getindex(mm,:Qramp));
      writedlm(@sprintf("sol%d_PQramps.txt", i), [pramp, qramp]);
    end
  end
end


function scopf_loadSolFromFile(stochasticBlock)
  if(stochasticBlock==0)
    a = readdlm("sol0_PGQG.txt")
    PG0 = a[1,:]; QG0 = a[2,:]
  
    a = readdlm("sol0_VaVm.txt")
    VA0 = a[1,:]; VM0 = a[2,:]

    #println(PG0, QG0, VA0, VM0
    return PG0, QG0, VA0, VM0
  else

    a = readdlm(@sprintf("sol%d_PQslacks.txt", stochasticBlock))
    P_slacki = a[1,:]; Q_slacki = a[2,:]

    a = readdlm(@sprintf("sol%d_VaVm.txt", stochasticBlock))
    VAi=a[1,:]; VMi=a[2,:]

    a = readdlm(@sprintf("sol%d_PQramps.txt", stochasticBlock))
    Prampi = a[1,:]; Qrampi = a[2,:]

    #println P_slacki, Q_slacki, VAi, VMi, Prampi, Qrampi);
    return P_slacki, Q_slacki, VAi, VMi, Prampi, Qrampi

  end    
  
end

function scopf_outputAll(scopfmodel, scopfdata)
  #shortcuts for compactness

  raw = scopfdata.raw
  opfdata = opf_loaddata(raw)
  lines_off = scopfdata.lines_off
  opf_data = opf_loaddata(raw)

  lines = opf_data.lines; buses = opf_data.buses; generators = opf_data.generators; baseMVA = opf_data.baseMVA
  busIdx = opf_data.BusIdx; FromLines = opf_data.FromLines; ToLines = opf_data.ToLines; BusGeners = opf_data.BusGenerators;

  nbus  = length(buses); nline = length(lines); ngen  = length(generators)

  # OUTPUTING
  println("Objective value: ", getobjectivevalue(scopfmodel), "USD/hr")
  VM=getvalue(getindex(scopfmodel,:Vm)); VA=getvalue(getindex(scopfmodel,:Va));
  PG=getvalue(getindex(scopfmodel,:Pg)); QG=getvalue(getindex(scopfmodel,:Qg));

  VM=VM[:,0]; VA=VA[:,0]; #base case

  # EX=getvalue(getindex(opfmodel,:Pramp));
  # EX=EX[:,0];

  # printing the first stage variables
  println("============================= BUSES ==================================")
  println("  Generator  |  ramp ")   # |    P (MW)     Q (MVAr)")  #|         (load)   ")  
  println("----------------------------------------------------------------------")
  # for i in 1:ngen
  #     @printf("  %10d | %6.2f \n",generators[i].bus, EX[i])
  # end
  println("\n")

  println("============================= BUSES ==================================")
  println("  BUS    Vm     Va   |   Pg (MW)    Qg(MVAr) ")   # |    P (MW)     Q (MVAr)")  #|         (load)   ") 
  
  println("                     |     (generation)      ") 
  println("----------------------------------------------------------------------")
  for i in 1:nbus
    @printf("%4d | %6.2f  %6.2f | %s  | \n",
	    buses[i].bus_i, VM[i], VA[i]*180/pi, 
	    length(BusGeners[i])==0?"   --          --  ":@sprintf("%7.2f     %7.2f", baseMVA*PG[BusGeners[i][1]], baseMVA*QG[BusGeners[i][1]]))
  end   
  println("\n")

  mm = getchildren(scopfmodel)[1]
  Psl=getvalue(getindex(mm,:P_slack)); Qsl=getvalue(getindex(mm,:Q_slack));
  println(Psl);
  return

  within=20 # percentage close to the limits
  
  
  nflowlim=0
  for l in 1:nline
    if lines[l].rateA!=0 && lines[l].rateA<1.0e10
      nflowlim += 1
    end
  end
  return

  if nflowlim>0 
    println("Number of lines with flow limits: ", nflowlim)

    optvec=zeros(2*nbus+2*ngen)
    optvec[1:ngen]=PG
    optvec[ngen+1:2*ngen]=QG
    optvec[2*ngen+1:2*ngen+nbus]=VM
    optvec[2*ngen+nbus+1:2*ngen+2*nbus]=VA

    d = JuMP.NLPEvaluator(opfmodel)
    MathProgBase.initialize(d, [:Jac])

    consRhs = zeros(2*nbus+2*nflowlim)
    MathProgBase.eval_g(d, consRhs, optvec)  


    #println(consRhs)

    @printf("================ Lines within %d %s of flow capacity ===================\n", within, "\%")
    println("Line   From Bus    To Bus    At capacity")

    nlim=1
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        flowmax=(lines[l].rateA/baseMVA)^2
        idx = 2*nbus+nlim
        
        if( (consRhs[idx]+flowmax)  >= (1-within/100)^2*flowmax )
          @printf("%3d      %3d      %3d        %5.3f%s\n", l, lines[l].from, lines[l].to, 100*sqrt((consRhs[idx]+flowmax)/flowmax), "\%" ) 
          #@printf("%7.4f   %7.4f    %7.4f \n", consRhs[idx], consRhs[idx]+flowmax,  flowmax)
        end
        nlim += 1
      end
    end
  end

  return 
end


# Compute initial point for IPOPT based on the values provided in the case data
function acopf_initialPt_IPOPT(opfdata)
  Pg=zeros(length(opfdata.generators)); Qg=zeros(length(opfdata.generators)); i=1
  for g in opfdata.generators
    # set the power levels in in between the bounds as suggested by matpower 
    # (case data also contains initial values in .Pg and .Qg - not used with IPOPT)
    Pg[i]=0.5*(g.Pmax+g.Pmin)
    Qg[i]=0.5*(g.Qmax+g.Qmin)
    i=i+1
  end
  @assert i-1==length(opfdata.generators)

  Vm=zeros(length(opfdata.buses)); i=1;
  for b in opfdata.buses
    # set the ini val for voltage magnitude in between the bounds 
    # (case data contains initials values in Vm - not used with IPOPT)
    Vm[i]=0.5*(b.Vmax+b.Vmin); 
    i=i+1
  end
  @assert i-1==length(opfdata.buses)

  # set all angles to the angle of the reference bus
  Va = opfdata.buses[opfdata.bus_ref].Va * ones(length(opfdata.buses))

  return Pg,Qg,Vm,Va
end


function main()
  mid, nprocs = getMyRank();
  solver = ARGS[1]; 
  prof = eval(parse(ARGS[2])); 
  
  if prof
    tic()  #t_user_model_loading
  end

  if prof
    tic() #t_rawdata_loading
  end

  raw = loadrawdata(ARGS[3]); 
  if prof
    t_rawdata_loading = toq()
    @message @sprintf("t_rawdata_loading %s", t_rawdata_loading)
  end



  # warm-start: existing solution files: load only (1), save only (2), load at the beginning AND save at the end (3), or do nothing (any other value)
  loadOrSave = parse(Int, ARGS[4])
  println("loadOrSave ================================== ", loadOrSave)

  # these are # of demand scenario per contingency
  num_demand_scens = parse(Int, ARGS[5]); 

  scopfmodel, scopfdata = scopf_model(raw, num_demand_scens)
  comm = getStructure(scopfmodel).mpiWrapper.comm
  # 
  # Initial point - needed especially for pegase cases
  #
  srand(10)
  if(loadOrSave==1 || loadOrSave==3)
    # warm-start similar to matpower
    scopf_init_x_fromFiles(scopfmodel,scopfdata,num_demand_scens)
    println("-- file based warm start")
  else
    scopf_init_x(scopfmodel,scopfdata,num_demand_scens)
    println("-- matpower based warm start")
  end

  if prof
    t_user_model_loading = toq()
    @message @sprintf(" t_user_model_loading %s ", t_user_model_loading)
  end

  t_user_model_loading_max = MPI.Reduce(t_user_model_loading, MPI.MAX, 0, comm)
  t_user_model_loading_min = MPI.Reduce(t_user_model_loading, MPI.MIN, 0, comm)
  t_user_model_loading_sum = MPI.Reduce(t_user_model_loading, MPI.SUM, 0, comm)
  if(MPI.Comm_rank(comm) == 0)
     @printf("max of t_user_model_loading is %s\n", t_user_model_loading_max)
     @printf("avg of t_user_model_loading is %s\n", t_user_model_loading_sum/MPI.Comm_size(comm))
     @printf("min of t_user_model_loading is %s\n", t_user_model_loading_min)
  end

  if prof
    tic() # t_solve
  end

  model,status = scopf_solve(scopfmodel,scopfdata, solver, prof)

  if prof
    t_solve = toq()
    @message @sprintf(" t_solve %s ", t_solve)
  end

  #if status==:Optimal
  #  scopf_outputAll(scopfmodel,scopfdata)
  #end
  if(loadOrSave==2 || loadOrSave==3)
    scopf_saveSolToFile(scopfmodel, scopfdata)
    println("-- solution saved to files")
  end
end
#run the case 9 example with 1 demand scenario, contingencies for lines 2 and 8, and save solution for each contingency (fifth parameter is 2)
### command line: julia scopf_stochDemand_structjump_busSlacks_v2_withRestart.jl PipsNlp false data/case1354pegase 2 1  2 8

#run the case 9 example with 16 demand scenarios, contingencies for lines 2 and 8, and load the solution that was previously saved (fifth parameter is 1) and use it to start the optimization. This solution is used of all the 16 demand scenarios within a contingency
### command line: julia scopf_stochDemand_structjump_busSlacks_v2_withRestart.jl PipsNlp false data/case1354pegase 1 16 2 8 
# or
### command line: mpirun -np 16 julia scopf_stochDemand_structjump_busSlacks_v2_withRestart.jl PipsNlp false data/case1354pegase 1 16 2 8 
main()
